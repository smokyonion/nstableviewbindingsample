//
//  CameraPreset.m
//  NSTableViewBindingSample
//
//  Created by Vincent Wayne on 6/15/12.
//  Copyright (c) 2012 madebycocoa. All rights reserved.
//

#import "CameraPreset.h"

@implementation CameraPreset

@synthesize name;
@synthesize camera;
@synthesize iso;
@synthesize isLegacy;

- (id)init
{
    self = [super init];
    if (!self) return nil;
    
    name = [[NSString alloc] initWithString:@"My Preset 1"];
    camera = [[NSString alloc] initWithString:@"CANON REBEL T3i"];
    iso = 400;
    isLegacy = NO;
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
	self = [[CameraPreset alloc] init];
    if (self) {
		self.name = [coder decodeObjectForKey:@"name"];
		self.camera = [coder decodeObjectForKey:@"camera"];
		self.iso = [coder decodeIntegerForKey:@"iso"];
        self.isLegacy = [coder decodeBoolForKey:@"isLegacy"];
	}
	
	return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeObject:name forKey:@"name"];
	[coder encodeObject:camera forKey:@"camera"];
	[coder encodeInteger:iso forKey:@"iso"];
    [coder encodeBool:isLegacy forKey:@"isLegacy"];
	
}

@end
