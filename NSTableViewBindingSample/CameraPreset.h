//
//  CameraPreset.h
//  NSTableViewBindingSample
//
//  Created by Vincent Wayne on 6/15/12.
//  Copyright (c) 2012 madebycocoa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CameraPreset : NSObject

@property (copy) NSString *name;
@property (copy) NSString *camera;
@property (assign) NSInteger iso;
@property (assign) BOOL isLegacy;

@end
